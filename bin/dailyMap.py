#!/usr/bin/env python

"""parses a day's worth of logs, making a json data file with only 1Hz positional data.
This is used to isolate race start stop times."""

import re, time, string, sys, math, argparse, glob, os
import simplejson as json #if no simple json, just import json
from decimal import *
from pytz import reference

import NMEA

#globals
log_dir = '/Users/grady/Projects/BoatComputer/data/raw'

if __name__ == '__main__':
    # set decimal library precision
    getcontext().prec = 12
    
    #parse options
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('date', type=str, help='date')

    args = parser.parse_args()
    
    logFiles = glob.glob( os.path.join(log_dir, args.date+'*') )

    maxLat = -180 
    minLat = 180
    maxLon = -180
    minLon = 180

    print "var track = ["

    for logFile in sorted(logFiles):

        with open( logFile, 'r' ) as f:
            for line in f:
                data = NMEA.parse(line)
                if data == None  or 't' not in data or data['t'].microsecond != 0:
                    continue

                data['t'] = int(data['t'].astimezone(reference.LocalTimezone()
).strftime("%s"))

                if 'lat' in data:
                    maxLat = max(maxLat, data['lat'])
                    minLat = min(minLat, data['lat'])
                    
                if 'lon' in data:
                    maxLon = max(maxLon, data['lon'])
                    minLon = min(minLon, data['lon'])

                print json.dumps(data, use_decimal=True), ","

    print "];"

    # print "var viewPort = ", json.dumps({})