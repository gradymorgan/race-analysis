#!/usr/bin/env python

"""This script renames files of the format YYMMDDHH.txt in UTC to the same format
in local time"""

import time, calendar
import os, glob

log_files = glob.glob('*')

for file_name in log_files:
    # print file_name
    t = calendar.timegm(time.strptime(file_name, '%y%m%d%H.TXT'))
    new_file_name = time.strftime('%y%m%d%H.txt', time.localtime(t))
    os.rename(file_name, new_file_name)
