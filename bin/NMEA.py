import string, sys, math

import pytz, datetime
from decimal import *

variation = 0

def coordToStr(coord):
    coord = abs(float(coord))
    s = str(int(math.floor(coord)))
    coord = (coord - math.floor(abs(coord))) * 60
    if coord < 10:
        s = s + '0'

    s = s + '%.4f'%(coord)
    return s

def checksum(sentence):
    """Calculate checksum for sentence; includes all characters in calculation."""
    csum = 0
    for c in sentence:
        csum = csum ^ ord(c)
    return "%02X" % csum

def verify(msg):
    """Verfiy that checksum on msg matches calculated checksum"""
    msg = msg.strip()
    return checksum(msg[1:-3]) == msg[-2:]


def RMB(dat):
    """ RMB Recommended Minimum Navigation Information

    Expected format:
                                                                14
           1   2 3    4    5       6 7        8 9  10  11  12 13 |
           |   | |    |    |       | |        | |   |   |   | |  |
    $--RMB,A,x.x,a,c--c,c--c,llll.ll,a,yyyyy.yy,a,x.x,x.x,x.x,A*hh

    1) Status, V = Navigation receiver warning 
    2) Cross Track error - nautical miles
    3) Direction to Steer, Left or Right
    4) TO Waypoint ID
    5) FROM Waypoint ID
    6) Destination Waypoint Latitude 
    7) N or S
    8) Destination Waypoint Longitude 
    9) E or W
    10) Range to destination in nautical miles 
    11) Bearing to destination in degrees True 
    12) Destination closing velocity in knots
    13) Arrival Status, A = Arrival Circle Entered 
    14) Checksum
    """

    if type(dat) is list:
        return None
    else:
        return "GPRMB,A,{xte:.2f},{dts},{from_wp},{to_wp},{lat},{latDir},{lon},{lonDir},{dtw:.1f},{btw:.0f},{vmgwp:.2f},V".format(
                xte = dat['xte'],
                dts = 'L',
                from_wp = dat['from'],
                to_wp = dat['to'],
                lat = coordToStr(dat['lat']),
                latDir = ('N' if dat['lat'] > 0 else 'S'),
                lon = coordToStr(dat['lon']),
                lonDir = ('E' if dat['lon'] > 0 else 'W'),
                dtw = dat['dtw'],
                btw = dat['btw'],
                vmgwp = dat['vmgwp']
            )
 
def RMC(words):
    """Parse RMC Message - Recommended Minimum Navigation Information
    
    Expected format:
                                                               12
                   1 2       3 4        5 6   7   8    9  10 11 |
                   | |       | |        | |   |   |    |   | |  |
    $--RMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,xxxx,x.x,a*hh

    1) Time (UTC) 
    2) Status, V = Navigation receiver warning 
    3) Latitude 
    4) N or S 
    5) Longitude 
    6) E or W 
    7) Speed over ground, knots 
    8) Track made good, degrees true 
    9) Date, ddmmyy
    10) Magnetic Variation, degrees 
    11) E or W 
    12) Checksum
    
    Example Messages:
    $GPRMC,204658,A,4740.2949,N,12228.5660,W,005.4,300.8,100410,018.2,E*63
    $GPRMC,225426,A,4741.9098,N,12225,2008,W,005.2,318.7,220412,018.0,E*65
    $GPRMC,152337,V,4741.0220,N,12224.3803,W,,,210412,018.2,E*74
    $GPRMC,030000.6,A,4740.36415,N,12225.35953,W,000.02,059.5,160612,016.6,E*4A
    
    """
    global variation
    
    lat_terms = words[3].split('.')
    lat = (1 if words[4]=='N' else -1)*int(lat_terms[0][:-2]) + Decimal(lat_terms[0][-2:]+'.'+lat_terms[1]) / Decimal('60.0')

    lon_terms = words[5].split('.')
    lon = (1 if words[6]=='E' else -1)*int(lon_terms[0][:-2]) - Decimal(lon_terms[0][-2:]+'.'+lon_terms[1]) / Decimal('60.0')
    
    ret = {'lat': lat, 'lon': lon}
    
    variation = (-1 if words[11]=='E' else 1) * Decimal(words[10])

    if len(words[7]) > 0:
        ret['sog'] = Decimal(words[7])

    if len(words[8]) > 0:
        ret['cog'] = Decimal(words[8])
    
    if len(words[1]) == 6:
        ret['t'] = datetime.datetime.strptime(words[9]+'T'+words[1], '%d%m%yT%H%M%S').replace(tzinfo=pytz.utc)
    else:
        ret['t'] = datetime.datetime.strptime(words[9]+'T'+words[1], '%d%m%yT%H%M%S.%f').replace(tzinfo=pytz.utc)

    return ret


def HDG(dat):
    """Parse HDG message - Heading, Deviation & Variation
    
    Expected format:
    
             1   2 3   4 5  6
             |   | |   | |  | 
    $--HDG,x.x,x.x,a,x.x,a*hh
    
    1) Magnetic Sensor heading in degrees
    2) Magnetic Deviation, degrees
    3) Magnetic Deviation direction, E = Easterly, W = Westerly
    4) Magnetic Variation degrees
    5) Magnetic Variation direction, E = Easterly, W = Westerly
    6) Checksum

    Example Messages:
    $IIHDG,298,,,18,E*18
    $HCHDG,192.5,0.0,E,,*26
    
    """
    if type(dat) is list:
        if len(dat[1]) > 0:
            hdg = Decimal(dat[1]) - variation
    
            return { 'hdg': hdg }
    else:
        return "GMHDG,%.1f,,,," % (dat.hdg)
    

def MWV(dat):
    """Parse MWV Message - Wind Speed and Angle
    
    Expected format:

             1 2   3 4  5
             | |   | |  | 
    $--MWV,x.x,a,x.x,a*hh
    
    1) Wind Angle, 0 to 360 degrees 
    2) Reference, R = Relative, T = True 
    3) Wind Speed 
    4) Wind Speed Units, K/M/N 
    5) Status, A = Data Valid 
    6) Checksum
    
    Example Messages:
    $CRMWV,35.5,R,,,A*7F
    $IIMWV,27.71,R,14.03,N,A*38
    
    """
    if type(dat) is list:        
        if dat[2] == 'R':
            output = {}

            if dat[1]:
                output['awa'] = Decimal(dat[1])
                
                if output['awa'] >= Decimal('360'):
                    raise Exception( "AWA greater than 360: "%(dat))
                
            if dat[3]:
                output['aws'] = Decimal(dat[3])
        
            return output
        
        return None
    
    else:
        return "GMMWV,%.f2,R,%.2f,N,A" % (dat['awa'], dat['aws'])

def MWD(dat):
    """Parse or Format MWD Message - Wind Direction and Speed, with respect to north
    
    Expected format:
    
             1 2   3 4   5 6   7 8  9
             | |   | |   | |   | |  | 
    $--MWD,x.x,T,x.x,M,x.x,N,x.x,M*hh
    
    1)  Wind direction, 0.0 to 359.9 degrees True, to the nearest 0.1 degree
    2)  T=True
    3)  Wind direction, 0.0 to 359.9 degrees Magnetic, to the nearest 0.1 degree
    4)  M = Magnetic 
    5)  Wind speed, knots, to the nearest 0.1 knot. 
    6)  N = Knots
    7)  Wind speed, meters/second, to the nearest 0.1 m/s. 
    8)  M = Meters/second
    9) Checksum
    """
    global variation

    if type(dat) is list:
        return None
    else:
        return "GMMWD,%.1f,T,%.1f,M,%.1f,N,," % ( dat['twd'], dat['twd']+variation, dat['tws'] )

def VHW(dat):
    """Parse VHW Message - Water Speed and Heading
    
    Expected Format:

             1 2   3 4   5 6   7 8  9
             | |   | |   | |   | |  | 
    $--VHW,x.x,T,x.x,M,x.x,N,x.x,K*hh

    1) Degress True 
    2) T = True 
    3) Degrees Magnetic 
    4) M = Magnetic 
    5) Knots (speed of vessel relative to the water) 
    6) N = Knots 
    7) Kilometers (speed of vessel relative to the water) 
    8) K = Kilometres 
    9) Checksum
    
    Example Messages:
    $CRVHW,,,,,6.1,N,,*3F
    $IIVHW,,,,,4.87,N,9.03,K*4D
    
    """
    if type(dat) is list:
        speed = Decimal(dat[5])
        return { 'speed': speed }
    else:
        return "GMVHW,,,,,%.2f,N,," % dat['speed']

def VDR(dat):
    """ Parse or format VDR Message - Set and Drift
    
    Expected Format:
             1 2   3 4   5 6  7
             | |   | |   | |  | 
    $--VDR,x.x,T,x.x,M,x.x,N*hh

    1) Degress True 
    2) T = True 
    3) Degrees Magnetic 
    4) M = Magnetic 
    5) Knots (speed of current) 
    6) N = Knots 
    7) Checksum
    
    """
    if type(dat) is list:
        return None
    else:
        return "GMVDR,,,{set:.1f},M,{drift:.2f},N".format(
                set=dat['set'], 
                drift=dat['drift']
            )
   
# $YXXDR,A,0.4,D,PTCH,A,-4.0,D,ROLL*70
def XDR(words):

    if len(words) > 6:
        return { 'pitch': Decimal(words[2]), 
                 'heel': Decimal(words[6]) }


def ROT(words):
    """Parse ROT Message - Rate Of Turn
    
    Expected Format:
             1 2  3 
             | |  | 
    $--ROT,x.x,A*hh
    
    1) Rate Of Turn, degrees per minute, "-" means bow turns to port 
    2) Status, A means data is valid 
    3) Checksum
    
    Example Messages:
    $TIROT,-20.3,A*27
    
    """
    rot = Decimal(words[1])
    return { 'rot': rot }
   
def format(msg):
    return "${0}*{1}\r\n".format(msg, checksum(msg))

exclude = []
def set_exclude(descriptors):
    global exclude 
    exclude = set(descriptors)

def parse(msg):
    msg = msg.strip()
    words = string.split(msg[1:-3], ',')
    if words[0] in exclude:
        return None

    try:
        if verify(msg):
            if words[0][2:] == "RMC" :
                return RMC(words)
                
            elif words[0][2:] == "HDG":
                return HDG(words)
                
            elif words[0][2:] == "MWV":
                return MWV(words)
                
            elif words[0][2:] == "VHW":
                return VHW(words)
                
            elif words[0][0:5] == "YXXDR":
                return XDR(words)
                
            elif words[0][0:5] == "TIROT":
                return ROT(words)

#             elif words[0][0:5] == "WIMWV":
#                 return ROT(words)    
           
            else:
                return None     
    
    except Exception as e:
        pass
        
    # sys.stderr.write('error parsing message: %s\n' % words)
    return None
