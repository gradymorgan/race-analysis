#!/usr/bin/env python

"""Import race from raw logs"""

import re, datetime, string, sys, math, argparse
import simplejson as json #if no simple json, just import json
from decimal import *
from pytz import reference

import NMEA

#globals
log_dir = '/Users/grady/Projects/BoatComputer/data/raw/'
js_dir = '/Users/grady/Projects/BoatComputer/race_analysis/web/analysis/races/'


def getTimeRangeForRace(date, startTime, endTime):
    #parse start time (-5 min) as local time
    startTime = datetime.datetime.strptime(date+'T'+startTime, '%Y%m%dT%H:%M').replace(tzinfo=reference.LocalTimezone())
    endTime = datetime.datetime.strptime(date+'T'+endTime, '%Y%m%dT%H:%M').replace(tzinfo=reference.LocalTimezone())
    
    return (startTime, endTime)

def getLogsForTimeRange(startTime, endTime):
    filePrefix = startTime.strftime('%y%m%d')
    
    files = []    
    for i in range(startTime.hour, endTime.hour+1):
        files.append(filePrefix + "{0:02d}".format(i) + ".txt")

    return files



if __name__ == '__main__':
    # set decimal library precision
    getcontext().prec = 12
    
    #parse options
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('regetta', type=str, help='regetta')
    parser.add_argument('date', type=str, help='date - YYYYMMDD')
    parser.add_argument('start_time', type=str, help='start time - HH:MM')
    parser.add_argument('end_time', type=str, help='end time - HH:MM')
    parser.add_argument('zoom', type=str, help='end time - HH:MM')
    parser.add_argument('center_lat', type=str, help='start time - HH:MM')
    parser.add_argument('center_lon', type=str, help='end time - HH:MM')


    args = parser.parse_args()
    

    (startTime, endTime) = getTimeRangeForRace(args.date, args.start_time, args.end_time)

    offset = (startTime - datetime.datetime(1970,1,1).replace(tzinfo=reference.LocalTimezone())).total_seconds()
    race_json = {
        'data': [],
        'meta': {
            'date': args.date,
            'start_time': args.start_time,
            'end_time': args.end_time
        },
        'view': {
            'lat': args.center_lat,
            'lon': args.center_lon,
            'zoom': args.zoom,
            'offset': int(offset)
        }
    }

    t = startTime - datetime.timedelta(100)

    maxLat = -180 
    minLat = 180
    maxLon = -180
    minLon = 180

    sec_data = None
    for log_file in getLogsForTimeRange(startTime, endTime):
        for line in open(log_dir + log_file, 'r'):
            data = NMEA.parse(line)
            if data == None:
                continue
                
            if 't' in data:
                t = data['t']

            if t < startTime:
                continue
                
            if 'lat' in data:
                maxLat = max(maxLat, data['lat'])
                minLat = min(minLat, data['lat'])
                
            if 'lon' in data:
                maxLon = max(maxLon, data['lon'])
                minLon = min(minLon, data['lon'])
    
    
            if 't' in data and data['t'].microsecond == 0:
                if t > endTime:
                    break
    
                if sec_data != None:
                    sec_data['t'] = (sec_data['t'] - startTime).total_seconds()
                    race_json['data'].append(sec_data)

                sec_data = {}



    
            sec_data.update(data)
        

    
    
    json.dump(race_json, sys.stdout, use_decimal=True, separators=(',\n', ':'))



