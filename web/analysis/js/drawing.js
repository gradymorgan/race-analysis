var variance = 0; 

function drawBoat( context, boat, size ) {
    size = size || 10;
    
    context.save();

    //alert(heading);
    context.translate(boat.pos.x, boat.pos.y);
    context.rotate( boat.heading * Math.PI / 180 );
    
    context.fillStyle = boat.color;

    context.beginPath();
    context.moveTo(0, -.8*size);
    context.bezierCurveTo( .6*size,0,
                           .5*size,.5*size,
                           .35*size,.8*size );
    context.lineTo(-.35*size, .8*size);
    context.bezierCurveTo( -.5*size,.5*size,
                           -.6*size,0,
                           0,-.8*size );

    context.closePath();
    context.fill();
    
    context.restore();
}

function boatOverlay(data, time) {
    
    this._color = '#AF1919';
    
    this._time = time;
    
    this._data = data;
    //this._tracklength = tracklength;
}

boatOverlay.prototype = new google.maps.OverlayView();

boatOverlay.prototype.onAdd = function() {
    var canvas = this._canvas = $('<canvas width="400px" height="400px">')[0];
    
    canvas.style.borderStyle = "none";
    canvas.style.borderWidth = "0px";
    canvas.style.position = "absolute";
    
    var panes = this.getPanes();
    panes.overlayImage.appendChild( canvas );
}

boatOverlay.prototype.draw = function() {
    // Size and position the overlay. We use a southwest and northeast
    // position of the overlay to peg it to the correct position and size.
    // We need to retrieve the projection from this overlay to do this.
    var overlayProjection = this.getProjection();
    
    if ( !this._canvas ) return;
    
    var context = this._canvas.getContext("2d");

    var boatIndex = 0;
    //TODO: binary search
    for ( var i = 0; i < this._data.length; i++ ) {
        if ( this._data[i].t >= this._time ) {
        
            boatIndex = i;
            break;
        }
    }

    var position = new google.maps.LatLng(this._data[boatIndex].lat, this._data[boatIndex].lon);
    var heading = this._data[boatIndex].hdg - variance;

    //center canvas around boat position
    var boatPosition = overlayProjection.fromLatLngToDivPixel(position);

    this._canvas.width = 40;
    this._canvas.height = 40;
    this._canvas.style.left = (boatPosition.x-20) + 'px';
    this._canvas.style.top = (boatPosition.y-20) + 'px';                    
    
    var boatSize = Math.min(Math.max(this.getMap().getZoom() - 9, 2), 10);
    drawBoat( context, 
                {
                    color: this._color,
                    pos: { x: 20, y: 20 },
                    heading: heading?heading:0
                }, 
                boatSize );
}

boatOverlay.prototype.setTime = function(time) {
    this._time = time;
    
    this.draw();
}